(function () {
  'use strict';
  angular.module('straus-street-select-example', [
    'ngResource',
    'ui.bootstrap',
    'straus-street-select'
  ]);
})();
(function () {
  'use strict';

  angular.module('straus-street-select-example')
    .config(function (strausStreetSelectSettings) {
      strausStreetSelectSettings.url = "http://localhost:8000/api/streets/autocomplete";
    });
})();
