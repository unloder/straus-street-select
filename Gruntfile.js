module.exports = function (grunt) {
  'use strict';
  grunt.config.init({
    concat: {
      dist: {
        files: {
          'straus-street-select.js': [
            'dev/straus-street-select.module.js',
            'dev/**/**/*.js'
          ],
          'straus_street-select.css': [
            'dev/straus_street-select.css'
          ],
          'dist/vendor.js': [
            'bower_components/lodash/dist/lodash.min.js',
            'bower_components/jquery/dist/jquery.min.js',
            'bower_components/bootstrap/dist/js/bootstrap.min.js',
            'bower_components/angular/angular.min.js',
            'bower_components/angular-resource/angular-resource.min.js',
            'bower_components/angular-bootstrap/ui-bootstrap.js',
            'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
            'bower_components/straus-street-select/straus-street-select.js',
            'bower_components/jquery-ui/jquery-ui.js',
            'bower_components/angular-ui-select/dist/select.js',
            'straus-street-select.js'
          ],
          'dist/vendor.css': [
            'bower_components/bootstrap/dist/css/bootstrap.min.css',
            'bower_components/angular-ui-select/dist/select.css'
          ]
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.registerTask('default', ['concat']);
};