/**
 * Created by unloder on 2/21/16.
 */
(function () {
  'use strict';
  angular.module('straus-street-select').factory('StreetsAutocomplete', function ($resource, strausStreetSelectSettings) {
    return $resource(strausStreetSelectSettings.url);
  });
})();