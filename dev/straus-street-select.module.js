(function () {
  'use strict';
  angular.module('straus-street-select', [
    'ui.select'
  ]);
})();