(function () {
  'use strict';
  angular.module('straus-street-select')
  .constant('strausStreetSelectSettings', {
    url: '',
    template:
    '<ui-select ng-cloak theme="bootstrap" ng-model="ngModel" reset-search-input="0" on-select="$select.search = $item;">'+
    '  <ui-select-match  placeholder="{{placeholder}}">{{$select.selected}}'+
    '  </ui-select-match>'+
    '  <ui-select-choices'+
    '    repeat="street in filterStreets($select.search) track by $index"'+
    '    refresh="autocompleteStreets($select)"'+
    '    refresh-delay="100">'+
    '    <div ng-bind="street"></div>'+
    '  </ui-select-choices>'+
    '</ui-select>'
  })
})();

