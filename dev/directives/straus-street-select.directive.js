(function () {
  'use strict';
  angular.module('straus-street-select').directive('strausStreetSelect', function (strausStreetSelectSettings, $timeout) {
    return {
      restrict: 'E',
      scope: {
        'ngModel': '=',
        'id': '@',
        'placeholder': "@"
      },
      link: function (scope, elem, attr) {
        if (scope.id){
          var $label = $('label[for="'+scope.id+'"]');
          if ($label){
            $label.off('click');
            $label.on('click',function(){
              $timeout(function() {
                var input = elem.find('input');
                if (attr.uiselectAutofocus == 'open')
                    input.click();
                input.focus()
            }, 0);
            });
          }
        }
      },
      template: strausStreetSelectSettings.template,
      controller: function($scope, StreetsAutocomplete, strausStreetSelectSettings){
        if (!strausStreetSelectSettings.url){
          console.log('The strausStreetSelect field is not configured properly, add the "strausStreetSelect.url" via config');
        }
        $scope.proposedStreets = [];
        $scope.isInit = false;

        $scope.filterStreets = function(search){
          var _streets = _.map($scope.proposedStreets, function(street){
            return street.name;
          });
          if (search && _streets.indexOf(search) === -1) {
            _streets.unshift(search);
          }
          return _.uniq(_streets);
        };

        $scope.autocompleteStreets = function ($select) {
          if (!$scope.isInit){
            $scope.isInit = true;
            $select.search = $scope.ngModel
          }else{
            $scope.ngModel = $select.search;
          }
          var query = $select.search;
          $select.selected = query;
          if (query && query.length > 2){
            StreetsAutocomplete.query({"query": query}).$promise.then(function (streets) {
              $scope.proposedStreets = streets;
            });
          }
        };
      }
    };
  });
})();